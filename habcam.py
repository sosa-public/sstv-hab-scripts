from picamera import PiCamera
import time

camera = PiCamera()
time.sleep(3)
camera.resolution = (1920, 1080)
camera.contrast = 10

while (1):
	t = time.localtime()
	current_time = time.strftime("%d%m%y_%H%M%S", t)
	file_name = "/home/pi/videos/video_" + str(current_time) + ".h264"
	#print("Recording started...")
	camera.start_recording(file_name)
	camera.wait_recording(10)
	camera.stop_recording()
	t = time.localtime()
	current_time = time.strftime("%d%m%y_%H%M%S", t)
	for i in range(1, 4):
		file_name = "/home/pi/images/img_" + str(current_time) + "_" + str(i) + ".jpg"
		camera.capture(file_name)
	 #print("Done..\n\n")
