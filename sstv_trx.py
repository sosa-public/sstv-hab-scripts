import RPi.GPIO as GPIO
import time
import serial

#DRA818 GPIO configuration
DRA818_PTT = 17
DRA818_SQ = 18 # Currently we ignore this. Pin 18 also appears to conflict with the PWM outputs used for audio.
DRA818_PD = 22 # Low by default - power saving mode

# Default Transmitter / Squelch Settings
MODE = 1 # 1 = FM (supposedly 5kHz deviation), 0 = NFM (2.5 kHz Deviation)
SQUELCH = 5 # Squelch Value, 0-8
CTCSS = '0000'

def dra818_program(port='/dev/ttyAMA0', frequency=144.500):
    ''' Program a DRA818U/V radio to operate on a particular frequency. '''


    _dmosetgroup = "AT+DMOSETGROUP=%d,%3.4f,%3.4f,%s,%d,%s\r\n" % (MODE, frequency, frequency, CTCSS, SQUELCH, CTCSS)

    print("Sending: %s" % _dmosetgroup.strip())

    # Open serial port
    _s = serial.Serial(
            port=port,
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS)
    # We need to issue this command to be able to send further commands.
    _s.write(("AT+DMOCONNECT\r\n").encode())
    time.sleep(1.00)
    _response = _s.readline()
    print("Connect Response: %s" % _response)

    # Send the programming command..
    _s.write(_dmosetgroup.encode())
    time.sleep(1.00)

    # Read in the response from the module.
    _response = _s.readline()
    _s.close()

    print("Response: %s" % _response.strip())

def dra818_ptt(enabled):
    # Set the DRA818's PTT on or off
    if enabled:
        GPIO.output(DRA818_PTT, GPIO.HIGH)
    else:
        GPIO.output(DRA818_PTT, GPIO.LOW)

if __name__ == '__main__':
	# Main program flow

	dra818_program() # Set DRA818 frequency

	# intial GPIO setup
	GPIO.setwarnings(False)
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(DRA818_PD, GPIO.OUT, initial=GPIO.LOW) # Set DRA818 to power saving mode
	GPIO.setup(DRA818_PTT, GPIO.OUT, initial=GPIO.LOW) # Disable PTT - DRA818 in RX mode 

	while(1):
		for i in range(1,5):
			print(i)
			time.sleep(1)
