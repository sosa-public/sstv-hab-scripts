# Python scripts for SSTV and camera high altitude balloon
- ``habcam.py``: Take video clips using a Raspberry Pi camera with still pictures in between.

- ``sstv_trx.py``: Transmit a set of pictures as SSTV via DRA818 radio module. Transmit morse message after every picture. Transmissions need to be prepared as audio files.

- ``webcam_server.py``: Raspberry Pi camera server on port 8000 for debugging and camera setup.

**Not final version! Use only for development!**